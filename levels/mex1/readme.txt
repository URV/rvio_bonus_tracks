Track inspired by Forza Horizon 5 (also set in Mexico)

Cartoon Tropical Nature Pack by Elegant Crow for texture rendering
https://elegantcrow.itch.io/cartoon-tropical-nature-pack

Metal Barriers are by .?

Textures come from Cliffside by R6TE and Images Of Giza by Skitch (Unreal Tournament 2004)

Skybox "HDRI 360° ocean italy-Livornese" by OpenFootage.net
https://www.openfootage.net/hdri-360-ocean-italy-pisa/

Waterfall Sound
https://www.freesoundslibrary.com/waterfall-sounds/

Pink Plant Woods Area 3 (Animal Life) by the late Rémi Gazel for Rayman 1 just using royalty free sounds (modified to loop)