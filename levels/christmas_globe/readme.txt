 
 Christmas Snow Globe - December 2009
 -------------------------------
 by jaseaka and hilaire9 
 -----------------------
 Length: 399 meters                                      
 Type: Extreme
 Folder name: christmas_globe
 ==================================================== 
 *** To Install: Unzip into your main Re-Volt folder. 
 ==================================================== 
 Description: Race around inside a Christmas Snow Globe. 
 -------------------------------------------------------
 Credit: Santa and sleigh models modified from Santa�s 
 Hot Rod Sleigh car by Iron Bob. 
 -------------------------------------------------------
 Tools: 3ds Max 8, ASE Tools, MAKEITGOOD 
 edit modes, and Paint Shop Pro 9.
 ----------------------------------------------------------
 --- jaseaka's MySpace page: http://www.myspace.com/jaseaka

 --- hilaire9's Home Page: http://members.cox.net/alanzia 
 
 
