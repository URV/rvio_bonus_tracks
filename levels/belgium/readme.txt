                                                                 May 2006
==========================================
Track name: Belgium
Author: hilaire9
Track length: 620 meters                                      
Type: Extreme
================================================
*** Track Installation:  Extract into your main Re-Volt folder
----------------------------------------------------
** Description:   Belgium is a real country and not just a part of France.  
                         It has a government and culture, everything a real country
                         has.  It is rarely in the news, but believe me it does exist.
                         It is famous for waffles, lace, and fried potato strips (called
                         French Fries in English because people have heard of France).
                         The most well known people born in Belgium are Hercule Poirot
                         and the great actor Jean-Claude Van Damme.
                                                                  ---Sir hilaire9, world traveler        
----------------------------------------------------
    
--------- Tools:  rvGlue, MAKEITGOOD edit modes, mkmirror, rv-Sizer and PSP7.
                       Built with ripped textured Lego floor pieces, also Bridge and Tree prms 
                       by Human, Lamp Post and Duck prms by RST,  Rock and Mountain
                       prms by Jimk, Rail and Post prms by Yamada, and Bush, Bench and 
                       Beam prms from Revolt.

--------- hilaire9's Home Page:  http://members.cox.net/alanzia
|||=================================================|||