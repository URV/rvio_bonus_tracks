
 Wipeout 2097
 ------------
 by hilaire9 and RST - November 2012
 -----------------------------------
 * To Install: Unzip into your main Re-volt folder.
 --------------------------------------------------
 For custom elements use latest Revolt update 1.2 by Huki.  
 ---------------------------------------------------------
 Track length: 455 meters
 Type: Extreme
 Folder name: wipeout2097
 -------------------------
 Credits: Strange tower models by rickyD.
 Music: Hand in Hand by X-Mal Deutschland.
 ----------------------------------------- 
 Tools: 3D Studio Max 8, ASE Tools, and Paint Shop Pro 9.
 --------------------------------------------------------
 --- My Home Page: www.hilaire9.com




