{
	SPARK {
  	ID              13                             ; Particle ID [0 - 63]
  	Name            "BLUE"                       ; Display name

  	CollideWorld    true                          ; Collision with the world
  	CollideObject   true                          ; Collision with objects
  	CollideCam      true                          ; Collision with camera
  	HasTrail        false                          ; Particle has a trail
  	FieldAffect     true                          ; Is affected by force fields
  	Spins           true                         ; Particle spins
  	Grows           true                        ; Particle grows
  	Additive        true                          ; Draw particle additively
  	Horizontal      false                         ; Draw particle flat

  	Size            1500.000000 1500.000000             ; Size of the particle
  	UV              0 0             ; Top left UV coordinates
  	UVSize          1.000 1.000             ; Width and height of UV
  	TexturePage     5                            ; Texture page
  	Color           255 255 255                   ; Color of the particle

  	Mass            -10.003000                      ; Mass of the particle
  	Resistance      0.000500                      ; Air resistance
 	Friction        0.900000                      ; Sliding friction
  	Restitution     0.200000                      ; Bounciness

  	LifeTime        0.500000                      ; Maximum life time
  	LifeTimeVar     0.500000                      ; Life time variance

  	SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  	SpinRateVar     0.000000                      ; Variation of the spin rate

  	SizeVar         10.000000                      ; Size variation
  	GrowRate        100.000000                      ; How quickly it grows
  	GrowRateVar     10.000000                      ; Grow variation

  	TrailType       1                             ; ID of the trail to use
  	}

	SPARK {
  	ID              14                             ; Particle ID [0 - 63]
  	Name            "BIGBLUE"                       ; Display name

  	CollideWorld    true                          ; Collision with the world
  	CollideObject   true                          ; Collision with objects
  	CollideCam      true                          ; Collision with camera
  	HasTrail        false                          ; Particle has a trail
  	FieldAffect     true                          ; Is affected by force fields
  	Spins           true                         ; Particle spins
  	Grows           true                        ; Particle grows
  	Additive        true                          ; Draw particle additively
  	Horizontal      false                         ; Draw particle flat

  	Size            10500.000000 10500.000000             ; Size of the particle
  	UV              0 0             ; Top left UV coordinates
  	UVSize          1.000 1.000             ; Width and height of UV
  	TexturePage     5                            ; Texture page
  	Color           255 255 255                   ; Color of the particle

  	Mass            -10.003000                      ; Mass of the particle
  	Resistance      0.000500                      ; Air resistance
 	Friction        0.900000                      ; Sliding friction
  	Restitution     0.200000                      ; Bounciness

  	LifeTime        0.500000                      ; Maximum life time
  	LifeTimeVar     0.500000                      ; Life time variance

  	SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  	SpinRateVar     0.000000                      ; Variation of the spin rate

  	SizeVar         10.000000                      ; Size variation
  	GrowRate        100.000000                      ; How quickly it grows
  	GrowRateVar     10.000000                      ; Grow variation

  	TrailType       1                             ; ID of the trail to use
  	}
}