    ___                    _            _     _              ______  _____ 
   / _ \                  | |          | |   (_)             | ___ \/  __ \
  / /_\ \_ __  _ __   __ _| | __ _  ___| |__  _  __ _ _ __   | |_/ /| /  \/
  |  _  | '_ \| '_ \ / _` | |/ _` |/ __| '_ \| |/ _` | '_ \  |    / | |    
  | | | | |_) | |_) | (_| | | (_| | (__| | | | | (_| | | | | | |\ \ | \__/\
  \_| |_/ .__/| .__/ \__,_|_|\__,_|\___|_| |_|_|\__,_|_| |_| \_| \_| \____/                                                     
        |_|   |_|      
									
                                 R E A D M E
                     Version 1.1, from November 8th 2022

  S U M M A R Y
═════════════════════════════════════════════════════════════════════════════
Track Name:         Appalachian RC
Folder Name:        appalachian
Author:             Jan Aínnír Mayen (kiwi)
Creation:           October 2022
Length:             305 meters
Difficulty:         Medium
Reversed Mode:      NO
Practice Star:      YES
Time Trial Time:    00:26:00
Racing Stars:       1


  T R A C K    D E S C R I P T I O N
═════════════════════════════════════════════════════════════════════════════

Welcome to Green Mountains, Vermont!

Grab your favourite Offroad RC-car or Monster Truck and join the races at
Appalachian RC, the new racing track built by the Green Mountain RC Club.

Club races every THU and SAT afternoon, 17 UTC. (April to October)
For special races and events please look at the billboard at the entrance.

FREE riding for club members, everyone else pay 2 $ per day.
Club membership 10 $ per season!

Find all details at geocities.com/appalachian-rc/2030
For the club website visit tripod.lycos.com/gmrcc-vt

For club membership call 1-800-GMRC-VT

Major bullets for Appalachian RC:

- Pseudo-realistic RC-car racing track
- Works best / most fun with Offroad RC-cars and Monster Trucks
- Raceline is based on Keyran's Rally PRM Kit
- Time Trial time to beat (Semi-Pro/Pro cars), Practice mode star to find
- Custom object and texture animations, custom surface properties
- Music track from Screamer 2 (c) 1996 by Virgin Interactive

Very special thanks to Javildesign, Mace, Skarma, Xarc and Zorbah for some 
assets which I used as a base. A full list can be found in the included
Readme.

Appalachian RC has no Reversed version available, but as an easteregg you can
race on the raw-version of the track instead! This version contains just the
PRM-pieces as provided by Keyran's Rally PRM Kit. This way you can see how I
started working on the track.

Have fun!



  C O M P A T I B I L I T Y
═════════════════════════════════════════════════════════════════════════════
+ This track is only compatible with RVGL 21.0930a or later. It's not
  compatible with the original (Vanilla) Re-Volt, Re-Volt 1.2, or the GOG
  and Steam Versions of Re-Volt.
+ For best performance and increased graphic quality, it's highly recommended
  to use the shader renderer. Set 'shaders = 1' in 'rvgl\profiles\rvgl.ini' 
  where 'rvgl' is the location of your RVGL installation.


  K N O W N    I S S U E S
═════════════════════════════════════════════════════════════════════════════
Issues caused by not using the shader renderer:

+ A much higher chances to get framedrops.
+ Graphical issues, especially flickering translucent faces and lights.


  C R E A T I O N
═════════════════════════════════════════════════════════════════════════════
Used Tools:      + Blender 2.79b with Marv's Re-Volt Plugin
                 + Ulead Photo Impact 12
                 + Audacity 2.2.2
                 + Notepad++ v8.1.9.1
				 + World Cut 11-11-11 by jigebren
                 + RVGL Makeitgood mode
				 
W/PRM Polygons:  ~ 65.000
NCP Polygons:    ~ 50.000

I used some meshes and textures from other sources as a base, and adjusted
them so they fitted my needs. They are listed below. Meshes and textures
which are not listed are made by me from scratch. Re-used models from the
original Re-Volt tracks (made by Probe/Acclaim) are also not listed.

Used meshes:    + Tent, Gates, Wheel, Chair, Workbench: RC Stadium 1 by Xarc
				    - https://www.revoltworld.net/dl/rc-stadium-1/
					- Used 3d models as a base
					- Used textures as a base
					
				+ Jeep: Toyota Land Cruiser (J100) by Skarma
					- https://www.revoltworld.net/dl/landcruiser100
					- Adjusted and downscaled texture
					- Slightly adjusted 3d model
					
				+ Plane: SE 2000 by Xarc
				    - https://www.revoltworld.net/dl/se-2000/
					- Downscaled texture
					- Slightly adjusted 3d model
					
				+ Big Display: The Thunderdrome by Mace2-0
				    - https://www.revoltworld.net/dl/tdrome/
					- Used texture as a base
					- Adjusted 3d model
					
				+ RC parts: Skating Toys Redux by Zorbah
				   - https://www.revoltworld.net/dl/skatoysred/
				   - Used 3d models as a base
				   - Used textures as a base
					

Used textures:  + Textures for the meshes as listed above
                + Skybox: Route-77 by javildesign
				+ Timer animation: The Thunderfrome by Mace2-0, adjusted
                  by Jan Aínnír Mayen (kiwi)

Used Sounds:    + birds1.wav			 bigsoundbank.com
                + birds2.wav             bigsoundbank.com
                + birds3.wav           	 bigsoundbank.com
                + lawnmowver.wav		 bigsoundbank.com

bigsoundbank.com: Licensed under a Creative Commons "Attr." 4.0 Licence

Used Music:

"Wise Up" by Atomiser (c) 1996, from the Screamer 2 Soundtrack


  T H A N K    Y O U
═════════════════════════════════════════════════════════════════════════════
Thank you to the original Probe developers for creating Re-Volt, and thank
you to Huki & the rest of the RVGL team for keeping the game alive 23 years
after its initial release.



  R E L E A S E   H I S T O R Y
═════════════════════════════════════════════════════════════════════════════
Version 1.0 from October 25th 2022
 + Release for the Re-Volt World Spooktober 2022 event (www.revoltworld.net)
 
Version 1.1 from November 8th 2022
 + Adjustment release
    - Adjusted trackzones to avoid skipping possibilities
	- Placed some obstacles to avoid skipping possibilities
	- Added a new instance (fence)
	- Adjusted the position of some pickups

 
  D I S C L A I M E R
═════════════════════════════════════════════════════════════════════════════
(1) My creations can be redistributed in any possible way without asking for
    permission, but please do not change any parts of it. Exception see (2).
	
(2) You are allowed to change any parts of my creations without asking for
    permission, if you use a different unique foldername.
	
(3) You can reuse any parts of my creations for your own creations, as long
    you mention the original authors accordingly in a readme-file.
	
(4) You are not allowed to use any parts of my creations for commercial
    purposes. 
	