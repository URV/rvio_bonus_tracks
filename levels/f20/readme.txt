For Re-Volts 20th Anniversary over on the RVIO discord we organized a meetup in Frankfurt. 
One of the things we did was race real RC cars on a skate park under a bridge. 
This is a recreation of that very skate park and it's surroundings. 
The graffiti has been changed to RV related things inside jokes plus everyone who attended has their username featured prominently too. 
Pictures we took are also littered around. 
I hope you enjoy this as much as we did racing those cars there but that's going to be difficult with the amazing time we had. 
Special thanks to those who attended the meetup and RV Pure and RV Passion for supplying the cars.
Check out our RC shenanigans on Youtube at https://www.youtube.com/watch?v=KCxkCOtanWQ