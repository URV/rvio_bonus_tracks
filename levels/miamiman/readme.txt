Map originally made by Rottking and Nautilus called Miami Manslaughter

Music:
TeraVex - Collateral Damage
TeraVex - Night of the Side
Ketsa - Aloft

Flo's cars featured:
Copacabana
Starline

"Street" by Daniel Simion from Soundbible mixed in from sounds from Driver 1/2 (assumed to be Sound Ideas Series 6000)

Fonts used for Graffiti:
a Attack Graffiti by wep
https://www.dafont.com/a-attack-graffiti.font

Zit Graffiti by Olivier "Zitoune" D.
https://www.dafont.com/zit-graffiti.font

Gang Bang Crime by Maelle.K
https://www.dafont.com/gang-bang-crime.font