This track is originally published by Nintendo Co. Ltd. for the game "Mario Kart Super Circuit" and then remade as a retro track for the game "Mario Kart Wii".
This is a coversion using the models from Mario Kart Wii and adapted to fit the size of RVGL cars. I do not own this track's model, textures or concept. 

Creator notes: This is the third of (I hope) multiple conversions from Mario Kart Wii.

Technical notes: The track has slightly altered ramps to allow for RVGL cars to drive and clear them.
You can drive wherever you want, even on the sides of the track if you really feel the need to. There is not much else to drive, and falling off is an instant repo.
You can jump over the wall to cut some time, if you manage to jump of course.
There are two time split instead of the usual 3 because the track is not as long as other tracks (30 seconds with toyeca).
I copied the item sets from MKWii, although in the original game the items that move around can't move in this game and they are static.
In this track there will always be 35 pickups (+ 1 star) regardless of the number of players. This allows for even matches where everyone can get an item. The side effect is that races will a bit more chaotic, but I prefer that to unfair.
You can race in reverse even though the original game never allowed so (maybe in one of the Mario Kart Channel Event). In reverse, the first item set was moved to be in front of the grid (instead of behind) and the last item was moved behind the grid. Also the ramps are reversed and drive exactly the same as the ramps normally.
This track is a medium track in lenght, but still shorter than usual and I would advise 4 laps instead of the usual 3. 
I rated this track as "Medium", since it's layout is easy to understand, but contains a few caveats that make it a step harder than other GBA tracks. 

I will repeat this more than one time: you can take whatever path fits you best. You can take the jump cut, there is no reposition trigger to prevent this. You can also jump over the walls to skip sections of the track, as far as track zones allow.

Thank you for playing RVGL in 2020 and for downloading this track. I hope you enjoy.

You may not publish an edited version this track without my permission, unless I've been out of the RVGL scene for a couple years. If I leave and return, I reserve any right to undo any edit you made to the track.
This track is part of the MKWii Conversion Project for RVGL. It will include (I hope) all the 32 stock tracks in Mario Kart Wii, complete with reverse version if possible (sorry, no reverse on Rainbow Road).