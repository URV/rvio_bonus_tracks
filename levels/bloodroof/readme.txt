 ++++++++++++++++++++++++++++++++++++++ Jan. 2011
 --- Track name: Blood on the Rooftops
 --- Authors: ubu and hilaire9 
 --- Track length: 621 meters
 --- Type: Extreme
 --- Folder name: bloodroof 
 +++++++++++++++++++++++++++++++++++++++++++++++++
  
 * To Install: Extract into your Re-volt directory. 
 
 +++++++++++++++++++++++++++++++++++++++++++++++++
 --- Description: Conversion of the Carmageddon 
 track Blood on the Rooftops.
 +++++++++++++++++++++++++++++++++++++++++++++++++
 ---Credit: Original Carmageddon track mesh 
 supplied by Harmalarm and Halogaland. Modified 
 for Revolt by ubu and hilaire9. All new textures, 
 simplified mesh and several new models.  
 ---Tools: 3D Studio Max 8, ASE Tools 
 and Paint Shop Pro 9.
 +++++++++++++++++++++++++++++++++++++++++++++++++
 