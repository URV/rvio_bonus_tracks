
BEST-MILK FARM
Version 2.0 from 19. November 2020

Created by:		Jan Aínnír Mayen
Created:		12. - 16. November 2020

Length:			389m
Difficulty:		Easy
Time trial:		00:23:00
Practice star:	Yes
Foldername:		bmfarm

Vis. Polygons:	~ 130.000
Col. Polygons:	~  14.000

____________________________________________________

Description:

Howdy,
...and welcome to the farm!

This track is based on the MK 8 version of the famous "Moo Moo Meadows", 
carefully adapted to fit the Re-Volt universe. 
Have fun! More conversions in the same style by me to follow.

Complete new shading!
Reverse version!
Animated cows!
All-new weapon: The tomato-bomb!
Action guaranteed!

Music: "Farm Animals" by 14 Foot 1
Licensed under a CC BY-NC-ND 4.0 International License

RVGL 20.0930a or later required

Thx for playing!

____________________________________________________

Developers:

Jan Aínnír Mayen:
- Modeling/Re-modeling
- Shading
- Lights
- Texture adjustments
- Skybox
- SFX editing
- Model animations
- Object placements
- Instance placements
- AI-nodes
- Position-nodes
- Track zones
- Cameras
- Triggers
- NCP's

Nintendo:
- Original mesh
- Original textures

Ray Koopa:
- Mesh/texture Rip

____________________________________________________

Used assets:

Cow model and textures: 
- Random Talking Bush (from the game: Bone -  Out from Boneville / The Great Cow Race)

Farm windmill model:
- Marv & r6te (from the track: Ranch)

Tree model and textures:
- Centrixe (from the game: FeralHeart)

Hot air balloon and windmill rotor blade models:
- Centrixe (from the game: Mario Kart 8)

Windmill SFX:
- Marv & r6te (from the track: Ranch)

Chirp SFX:
- Kiwi (from the track: Beehive Valley Adventures)

Cow moo and cow bell SFX:
- Sounddogs.com

Re-Volt sponsor logos:
- Re-Volt Discord #resources channel

____________________________________________________

Thanks:

To whole Re-Volt Discord #tracks channel for your help, but especially Gotolei, javildesign and Vas0sky.

Testing:
- Durtvan
- EstebanMz
- hajducsekb
- javildesign
- Zeino

Music suggestion:
- Potato Face

____________________________________________________

Music:

"Farm Animals" by 14 Foot 1 

Copyright: Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) https://creativecommons.org/licenses/by-nc-nd/4.0/

URL: http://freemusicarchive.org/music/14_Foot_1/AS220s_Winter_Sampler_2013/01_-_14_Foot_1_-_Farm_Animals
