   ___         __   __                     __  __  ___                  _               ___ 
  / _ ) ___ _ / /_ / /_ ___  ____ ___  ___/ / /  |/  /___ _ ___   ___  (_)___   ___    |_  |
 / _  |/ _ `// __// __// -_)/ __// -_)/ _  / / /|_/ // _ `// _ \ (_-< / // _ \ / _ \  / __/ 
/____/ \_,_/ \__/ \__/ \__//_/   \__/ \_,_/ /_/  /_/ \_,_//_//_//___//_/ \___//_//_/ /____/ 
                                                                                           
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
hi there, this is the second part of the Battered Mansion. personally i find it a bit harder than the first. in this track you are driving both inside and outside the mansion. there are always multiple ways to choose from so the track nearly always feels different to play through. again i know about the poor ai competetivity but i did what i could. i hope you enjoy and have fun as much as i did.


credits:
music is syrsa - shard - night lights
textures from internet
https://freesound.org/people/sad3d/sounds/500169/
https://freesound.org/people/andersmmg/sounds/517883/