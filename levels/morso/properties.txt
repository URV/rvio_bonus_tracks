	MATERIAL {
	  ID              19         
	  Name            "Snow Surface"    
	  Color           135 99 73  
	  Skid            true      
	  Spark           true      
	  OutOfBounds     false      
	  Corrugated      true       
	  Moves           false      
	  Dusty           true       
	  Roughness       1 
	  Grip            1
	  Hardness        0 
	  DefaultSound    67         
	  SkidSound       68         
	  ScrapeSound     5          
	  SkidColor       225 225 225
	  CorrugationType 2          
	  DustType        4          
	  Velocity        0 0 0
	}
	
	MATERIAL {
	ID              20                           
	Name            "DIRT3"                       
	Color           66 40 25                     

	Skid            false                        
	Spark           false                         
	OutOfBounds     false                        
	Corrugated      true                          
	Moves           false                         
	Dusty           true                          

	Roughness       1.000000                    
	Grip            1.000000                      
	Hardness        0.000000                      

	DefaultSound    -1                            
	SkidSound       7                             
	ScrapeSound     5                             

	SkidColor       136 187 221                   
	CorrugationType 7                             
	DustType        6                             
	Velocity        0.000000 0.000000 0.000000    
	}
	
        DUST {
	  ID              4     
	  Name            "Snow Dust"
	  SparkType       31     
	  ParticleChance  0.4
	  ParticleRandom  0.8
	  SmokeType       28    
	  SmokeChance     0.2
	  SmokeRandom     0.8
	}
	
	        DUST {
	  ID              6     
	  Name            "Dirt"
	  SparkType       9     
	  ParticleChance  0.4
	  ParticleRandom  0.8
	  SmokeType       28    
	  SmokeChance     0.2
	  SmokeRandom     0.8
	}
	
		SPARK {
		ID              17                             
		Name            "EXPLOSION1"                      

		CollideWorld    false                         
		CollideObject   false                         
		CollideCam      false                         
		HasTrail        false                         
		FieldAffect     false                         
		Spins           false                         
		Grows           false                         
		Additive        true                          
		Horizontal      false                         

		Size            3.000000 3.000000             
		UV              0.937500 0.000000             
		UVSize          0.062500 0.062500             
		TexturePage     47                           
		Color           255 255 255                   

		Mass            0.100000                      
		Resistance      0.020000                      
		Friction        0.100000                      
		Restitution     0.500000                      

		LifeTime        0.300000                      
		LifeTimeVar     0.020000                      

		SpinRate        0.000000                      
		SpinRateVar     0.000000                      

		SizeVar         0.000000                      
		GrowRate        0.000000                      
		GrowRateVar     0.000000                      

		}

        SPARK {
	  ID              31              
	  Name            "Snow Particles"         
	  CollideWorld    true           
	  CollideObject   true           
	  CollideCam      false          
	  HasTrail        false          
	  FieldAffect     true           
	  Spins           false          
	  Grows           false          
	  Additive        true          
	  Horizontal      false          
	  Size            3.5 3.5
	  UV              0 0
	  UVSize          1 1
	  TexturePage     6             
	  Color           255 255 255    
	  Mass            0.001       
	  Resistance      0.001      
	  Friction        0.01     
	  Restitution     0.2      
	  LifeTime        1      
	  LifeTimeVar     0.2    
	  SpinRate        0    
	  SpinRateVar     25      
	  SizeVar         0.2      
	  GrowRate        0      
	  GrowRateVar     0     
	  TrailType       -1             
	}

	SPARK {
	  ID              3
	  Name            "Smoke"
	  CollideWorld    false
	  CollideObject   false
	  CollideCam      false
	  HasTrail        false
	  FieldAffect     false
	  Spins           true
	  Grows           true
	  Additive        true
	  Horizontal      false
	  Size            100 100
	  UV              0 0
	  UVSize          0.25 0.25
	  TexturePage     47
	  Color           10 10 10
	  Mass            0.01
	  Resistance      0.0001
	  Friction        0.001
	  Restitution     0.5
	  LifeTime        4
	  LifeTimeVar     0.5
	  SpinRate        0
	  SpinRateVar     0
	  SizeVar         10
	  GrowRate        400
	  GrowRateVar     50
         }
		 
		 SPARK {
		ID              11                            
		Name            "SMOKE2"                     

		CollideWorld    true                          
		CollideObject   true                          
		CollideCam      false                        
		HasTrail        false                         
		FieldAffect     false                         
		Spins           true                       
		Grows           true                         
		Additive        true                          
		Horizontal      false                        

		Size            110.000000 110.000000         
		UV              0.000000 0.000000             
		UVSize          0.25 0.25
		TexturePage     47                           
		Color           10 10 10                      

		Mass            0.030000                      
		Resistance      0.020000                    
		Friction        0.001000                      
		Restitution     0.000000                     

		LifeTime        1.600000                      
		LifeTimeVar     0.000000                      

		SpinRate        0.000000                      
		SpinRateVar     2.000000                      

		SizeVar         0.000000                     
		GrowRate        40.000000                    
		GrowRateVar     20.000000                     

		}
		
		SPARK {
	ID              9                            
	Name            "DIRT"                        

	CollideWorld    true                         
	CollideObject   true                        
	CollideCam      false                        
	HasTrail        false                        
	FieldAffect     true                        
	Spins           false                         
	Grows           false                        
	Additive        false                         
	Horizontal      false                        

	Size            3.500000 3.500000           
	UV              0 0             
	UVSize          1 1           
	TexturePage     9                           
	Color           150 150 150                 

	Mass            0.030000                      
	Resistance      0.001000                     
	Friction        0.400000                      
	Restitution     0.200000                     

	LifeTime        1.500000                      
	LifeTimeVar     0.500000                     

	SpinRate        0.000000                      
	SpinRateVar     25.000000                    

	SizeVar         0.000000                      
	GrowRate        0.000000                      
	GrowRateVar     0.000000                      
	}

	PICKUPS {
        SpawnCount      10 1                       
        EnvColor        255 255 128                   
        LightColor      98 66 0                     
        }