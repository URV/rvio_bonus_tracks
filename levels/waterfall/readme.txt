readme of Waterfall

release date: May 2017
authors: Instant; Mikrosx
compatible with: rvgl only
type: extreme
length: 248 meters
reversed mode: not available
practice star: not available
used tools: Blender, GIMP, FL Studio, makeitgood
zip and folder name: waterfall

contact me on Discord: Instant#4105

changelog:

2018/09/17
reverted texture
compressed wav file

2018/08/23
converted textures to jpg
removed excess files

2017/09/13
fixed camera bugs