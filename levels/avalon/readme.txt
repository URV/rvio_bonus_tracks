
 Avalon  
 by hilaire9 - September 2013
 * This track requires Revolt version 1.2 to display the 512x512 textures.
 Length: 665m                                     
 Type: Extreme
 Folder name: avalon
 Custom music, animated models and skybox with Revolt version 1.2 
 ================================================================
 * To Install: Extract into your Re-Volt folder.
 =================================================================
 Tools: 3D Studio Max 2010, ASE Tools, mkmirror, PaintShop Pro X5 
 and MAKEITGOOD edit modes. 
 hil's Home Page: http://www.hilaire9.com  
 =================================================================
 